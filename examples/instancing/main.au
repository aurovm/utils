
module _Box (Box_arg) {}
module _Box_export = {
    `` = Box_shell;
    make = Box_make;
    get = Box_get;
};

// Box body start

    // It's a quirk of Auro. I need to pass the argument so that Auro builds
    // a shell module per build of Box
    type Box_T = Box_arg.T;
    module Box_shell_mod = (import auro.typeshell)({ `0` = Box_T; _arg = Box_arg; });
    type Box_shell = Box_shell_mod.``;
    Box_T Box_get (Box_shell) = Box_shell_mod.get;
    Box_shell Box_make (Box_T) = Box_shell_mod.`new`;

// Box body end

module sarr_mod = (import auro.array)({ `0` = string; });
type sarr = sarr_mod.``;
sarr sarr_new (string, int) = sarr_mod.`new`;
sarr shell_names () { return sarr_new("T", 1); }

module Box = (import utils.modtpeq)({ base = _Box; names = shell_names; });



// Create a "dependency" module.

module ABox_mod = Box({ T = string; });
type ABox = ABox_mod.``;
ABox ABox_make (string) = ABox_mod.make;

ABox _a_box () { return ABox_make("Hello A Box!"); }

// Store the function in a module so Auro loses the function signature and cannot type check
module A = { box = _a_box; };


// Create a "main" module

// If Box wasn't wrapped in a modtpeq, this should create a separate module,
// which would make the types ABox and BBox distinct
module BBox_mod = Box({ T = string; });
type BBox = BBox_mod.``;
string box_get (BBox) = BBox_mod.get;

// This is _a_box but with a different signature, which should be compatible
BBox a_box () = A.box;

void main () {
    println(box_get(a_box()));
}

// Export only main
module exported = { main = main; };
export exported;