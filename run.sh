function build {
  echo build $1
  auro aulang "src/$1.au" "dist/utils.$1"
}

function build-legacy {
  echo build legacy $1
  auro aulang "legacy/src/$1.au" "dist/auro.utils.$1"
}

if [ "$1" == "build" -o "$1" == "b" ]; then
    if [ -n "$2" ]; then
        build "$2"
    else
        # Complex, but necessary for the collection modules
        build modeq &&
        build modtpeq &&

        build arraylist &&
        build alist &&
        build hashmap &&
        build stringmap &&

        build protocol &&
        build interface &&

        build-legacy arraylist &&
        build-legacy stringmap
    fi
fi

NUGGET_BIN=nugget

function run_test {
  TEST_FILE=$1

  # if test_file errored, print the messahe
  if [ $? == 1 ]; then
    echo $TEST_FILE
    exit 1
  fi

  DIST=tests/dist
  OUT=$DIST/main

  # Make sure the directory exists and it's empty
  if [ ! -d $DIST ]; then
    mkdir $DIST
  elif [ -n "`ls $DIST`" ]; then
    rm $DIST/*
  fi

  # Compile dependencies, if any
  USES=`cat $TEST_FILE | sed -n 's/.*\/\/@use \(.*\)/\1/p'`
  for DEP in $USES; do
    echo compiling $DEP
    $NUGGET_BIN aulang.v3 tests/deps/$DEP.au $DIST/$DEP || exit
  done

  # Compile the main file
  echo compiling $TEST_FILE
  $NUGGET_BIN aulang.v3 -L $DIST $TEST_FILE $OUT || exit

  # If no run, only print the resulting module
  if grep -q -F "//@norun" "$TEST_FILE"; then
    aurodump $OUT
  else
    # Collect expected output, if any
    EXPECTED=`cat $TEST_FILE | sed -n 's/.*\/\/@expect \(.*\)/\1/p'`

    # TODO: These two cases shouldn't be handled differently.
    # This is matching the output to the expected only if there's anything expected
    if [[ ! -z "$EXPECTED" ]]; then
      RESULT=`$NUGGET_BIN --dir dist $OUT`
      if [[ ! "$RESULT" == "$EXPECTED" ]]; then
        echo Test Failed
        #echo "Output:  " $RESULT
        #echo "Expected:" $EXPECTED
        diff --color <(echo "$EXPECTED") <(echo "$RESULT") 
        exit 1
      fi
    else
      $NUGGET_BIN --dir dist $OUT || exit $!
    fi

    echo "Test run succesfully"
  fi
}


if [ "$1" == "test" ]; then
    for file in `ls tests`; do
        file=tests/$file
        if [ -f $file ]; then
            run_test $file
        fi
    done

fi

if [ "$1" == "install" -o "$1" == "i" ]; then
    if [ -n "$(ls -A dist)" ]; then
        pushd dist
        for F in *; do
            auro --install "$F"
        done
    fi
fi


if [ "$1" == "example" ]; then
    TEST=$2
    DIR="examples/$TEST"
    if [ ! -d $DIR ]; then
        echo $2 is not an example
        exit 1
    fi

    if [ -d examples/dist ]; then
        rm examples/dist/*
    else
        mkdir examples/dist
    fi

    for FILE in $DIR/*; do
        MODNAME=$(basename "${FILE%.*}")
        auro aulang $FILE examples/dist/example.$TEST.$MODNAME
        if [ $? -ne 0 ]; then
            echo failed to compile $FILE
            exit
        fi
    done

    export LD_LIBRARY_PATH=$PWD/../nugget:$LD_LIBRARY_PATH
    ../nugget/nugget --dir dist --dir examples/dist example.$TEST.main
fi