module main (argument) {
module _export {
  `` = Self;
  `new` = _new;
  get = get;
  set = set;

  iterator = Iter;
  `new`$iterator = newIter;
  next$iterator = nextIter;
}
export _export;

module node_mod = (import auro.`record`)(module { `0` = K; `1` = V; `2` = Shell; });
module nulv_mod = (import auro.`null`)(module { `0` = V; });
module nuln_mod = (import auro.`null`)(module { `0` = Node; });
module rec_mod = (import auro.`record`)(module { `0` = NulN; });
module tp_mod = (import auro.typeshell)(module { `0` = Rec; });
module shell_mod = (import auro.typeshell)(module { `0` = NulN; });

type K = argument.K;
type V = argument.V;

bool eq (K, K) = argument.eq;

type Node = node_mod.``;
type NulN = nuln_mod.``;
type NulV = nulv_mod.``;
type Rec = rec_mod.``;
type Self = tp_mod.``;

type Shell = shell_mod.``;
NulN fromShell (Shell) = shell_mod.get;
Shell toShell (NulN) = shell_mod.`new`;

NulV newNulV () = nulv_mod.`null`;
NulN newNulN () = nuln_mod.`null`;
NulV toNulV (V) = nulv_mod.`new`;
NulN toNulN (Node) = nuln_mod.`new`;

Rec newRec (NulN) = rec_mod.`new`;
Self newSelf (Rec) = tp_mod.`new`;

Rec getRec (Self) = tp_mod.get;
bool isNulN (NulN) = nuln_mod.isnull;
Node getNulN (NulN) = nuln_mod.get;

NulN get_first (Rec) = rec_mod.get0;
void set_first (Rec, NulN) = rec_mod.set0;

Node newNode (K, V, Shell) = node_mod.`new`;
K get_k (Node) = node_mod.get0;
V get_v (Node) = node_mod.get1;
void set_v (Node, V) = node_mod.set1;
Shell get_next (Node) = node_mod.get2;
void set_next (Node, Shell) = node_mod.set2;


Self _new () { return newSelf(newRec(newNulN())); }

NulV get (Self this, K k) {
  NulN nul = get_first(getRec(this));

  while (true) {
    if (isNulN(nul)) {
      return newNulV();
    }
    Node node = getNulN(nul);
    if (eq(k, get_k(node))) {
      V v = get_v(node);
      return toNulV(v);
    }
    nul = fromShell(get_next(node));
  }
}

void set (Self this, K k, V v) {
  Rec rec = getRec(this);
  NulN first = get_first(rec);
  if (isNulN(first)) {
    Node node = newNode(k, v, toShell(newNulN()));
    set_first(rec, toNulN(node));
  } else {
    Node node = getNulN(first);
    while (true) {
      if (eq(k, get_k(node))) {
        set_v(node, v);
        return;
      } else {
        NulN next = fromShell(get_next(node));
        if (isNulN(next)) {
          Node next = newNode(k, v, toShell(newNulN()));
          set_next(node, toShell(toNulN(next)));
          return;
        } else {
          node = getNulN(next);
        }
      }
    }
  }
}

// Iterator

module pair = (import auro.`record`)(module { `0` = K; `1` = V; });
module nulpair = (import auro.`null`)(module { `0` = Pair; });
module iter_rec = (import auro.`record`)(module { `0` = NulN; });
module iter = (import auro.typeshell)(module { `0` = IterRec; });

type Pair = pair.``;
type NulPair = nulpair.``;

NulPair nullPair () = nulpair.`null`;
NulPair newNulPair (Pair) = nulpair.`new`;
Pair newPair (K, V) = pair.`new`;

type IterRec = iter_rec.``;
type Iter = iter.``;

IterRec newIterRec (NulN) = iter_rec.`new`;
NulN getIterInner (IterRec) = iter_rec.get0;
void setIterInner (IterRec, NulN) = iter_rec.set0;

Iter toIter (IterRec) = iter.`new`;
IterRec toIterRec (Iter) = iter.`get`;

Iter newIter (Self map) {
  Rec rec = getRec(map);
  NulN first = get_first(rec);
  IterRec rec = newIterRec(first);
  return toIter(rec);
}

NulPair nextIter (Iter this) {
  IterRec rec = toIterRec(this);
  NulN nuln = getIterInner(rec);
  if (isNulN(nuln)) {
    return nullPair();
  } else {
    Node node = getNulN(nuln);
    setIterInner(rec, fromShell(get_next(node)));
    return newNulPair(newPair(get_k(node), get_v(node)));
  }
}
}
export main;