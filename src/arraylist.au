
// == Shell ==

module sarr_mod = (import auro.array)(module { `0` = string; });
type sarr = sarr_mod.``;
sarr sarr_new (string, int) = sarr_mod.`new`;
sarr shell_names () { return sarr_new("0", 1); }

module shelled = (import utils.modtpeq)(module { base = main; names = shell_names; });
export shelled;

module main (argument) {
module _export {
  `` = self;
  `new` = create;
  len = len;
  get = get;
  push = push;
  set = set;
  insert = insert;
  remove = remove;
}
export _export;

module system = import auro.system;
void error (string) = system.error;

module array_mod = (import auro.array)(module { `0` = base; });
module nul_mod = (import auro.`null`)(module { `0` = array; });
module rec_mod = (import auro.`record`)(module { `0` = nul; `1` = int; });
module shell_mod = (import auro.typeshell)(module { `0` = rec; });

type base = argument.`0`;

type array = array_mod.``;
type nul = nul_mod.``;
type rec = rec_mod.``;
type self = shell_mod.``;

nul new_nul () = nul_mod.`null`;
nul to_nul (array) = nul_mod.`new`;
bool is_nul (nul) = nul_mod.isnull;
array get_nul (nul) = nul_mod.get;

rec rec_new (nul, int) = rec_mod.`new`;
void rec_set (rec, nul) = rec_mod.set0;
nul rec_get (rec) = rec_mod.get0;
void rec_set1 (rec, int) = rec_mod.set1;
int rec_get1 (rec) = rec_mod.get1;

self shell_new (rec) = shell_mod.`new`;
rec shell_get (self) = shell_mod.get;

array arr_new (base, int) = array_mod.`new`;
base arr_get (array, int) = array_mod.get;
void arr_set (array, int, base) = array_mod.set;
int arr_len (array) = array_mod.len;

self create () { return shell_new(rec_new(new_nul(), 0)); }
base get (self this, int i) {
  int count = len(this);
  if (i >= count) { error("index "+itos(i)+" out of bounds. len="+itos(count)); }

	array value = get_nul(rec_get(shell_get(this)));
	return arr_get(value, i);
}

int len (self this) {
  return rec_get1(shell_get(this));
}

void push (self this, base val) {
  insert(this, len(this), val);
}

void set (self this, int i, base val) {
  int count = len(this);
  if (i >= count) { error("index " + itos(i) + " out of bounds"); }

  array arr = get_nul(rec_get(shell_get(this)));
  arr_set(arr, i, val);
}

void insert (self this, int ix, base val) {
  int count = len(this);
  if (ix >= count+1) { error("index " + itos(ix) + " out of bounds"); }

  rec this = shell_get(this);
  if (is_nul(rec_get(this))) {
    // Start with capacity 32
    rec_set(this, to_nul(arr_new(val, 32)));
  } else {
    array old = get_nul(rec_get(this));
    array ne = old;

    int capacity = arr_len(old);
    if (count == capacity) {

      // Reallocate with bigger capacity
      capacity = 3*capacity / 2;
      ne = arr_new(val, capacity);
      rec_set(this, to_nul(ne));

      // Copy everything before the inserted element
      int i = 0;
      while (i < ix) {
        arr_set(ne, i, arr_get(old, i));
        i = i+1;
      }
    }

    // Shift (copy if reallocated) all elements after the insert.
    // Traverse reversed to not just copy the first element
    int i = count;
    while (i > ix) {
      i = i-1;
      arr_set(ne, i+1, arr_get(old, i));
    }

    // Insert the new element
    arr_set(ne, ix, val);
  }

  // Update the length
  rec_set1(this, count+1);
}

void remove (self this, int ix) {
  int count = len(this);
  rec this = shell_get(this);
  array arr = get_nul(rec_get(this));

  if (ix >= count) { error("index " + itos(ix) + " out of bounds"); }

  int i = 0;
  while (i < (count-1)) {
    int j = i;
    if (i >= ix) { j = j+1; }
    arr_set(arr, i, arr_get(arr, j));
    i = i+1;
  }

  rec_set1(this, count-1);
}
}